<html><head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="lvhcxQ5CGMX9ECofpi4JOUCr6IMMK5nE4B50eUex">

<title>Danh sách nhãn - tag</title>

<link href="http://cms.banhcuonphuonganh.com.vn/themes/inspinia/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="http://cms.banhcuonphuonganh.com.vn/themes/inspinia/css/animate.css" rel="stylesheet">

<link href="http://cms.banhcuonphuonganh.com.vn/themes/inspinia/css/style.css" rel="stylesheet">
<link href="http://cms.banhcuonphuonganh.com.vn/assets/cms/css/style.css" rel="stylesheet">

<link rel="shortcut icon" href="favicon.ico">    <script src="http://cms.banhcuonphuonganh.com.vn/themes/inspinia/js/jquery-2.1.1.js"></script>
<script src="http://cms.banhcuonphuonganh.com.vn/assets/base.js"></script>

<script>
    var csrf_token = 'lvhcxQ5CGMX9ECofpi4JOUCr6IMMK5nE4B50eUex';
</script>

</head>

<body>
    <div id="wrapper">

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
<!--                    <span>
                            <img alt="image" class="img-circle" src="" width="48px"
                                 height="48px"/>
                             </span>-->
                             <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">Admin</strong>
                                </span> <span class="text-muted text-xs block">Thông tin tài khoản <b class="caret"></b></span> </span> </a>
                                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                    <li><a href="http://cms.banhcuonphuonganh.com.vn/user/7/edit">Thông tin cá nhân</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li><a href="/logout">Thoát</a></li>
                                </ul>
                            </div>
                            <div class="logo-element">
                                CMS
                            </div>
                        </li>
                        <li class="">
                            <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Sản phẩm</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="/product">Sản phẩm</a></li>
                                <li><a href="/product/category">Thể loại Sản phẩm</a></li>
                                <li><a href="/product/feature">Sản phẩm Chọn lọc</a></li>
                                <li><a href="/product/hot">Sản phẩm Hot</a></li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Bài viết</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="/article">Bài viết</a></li>
                                <li><a href="/article/category">Thể loại Bài viết</a></li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Tin tức</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="/news">Tin tức</a></li>
                                <li><a href="/news/category">Thể loại Tin tức</a></li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Người dùng</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="/user">Quản trị viên</a></li>
                                <li><a href="/group">Nhóm Quản trị</a></li>
                            </ul>
                        </li>
                        <li class="active">
                            <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Thông tin khác</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse in">
                                <li><a href="/menu">Menu</a></li>
                                <li><a href="/slide">Slide</a></li>
                                <li><a href="/tag">Từ khóa</a></li>
                                <li><a href="/faq">Hỏi đáp</a></li>
                                <li><a href="/config">Cấu hình</a></li>
                            </ul>
                        </li>
                    </ul>

                </div>
            </nav>
            <div id="page-wrapper" class="gray-bg dashbard-1">
                <div class="row border-bottom">

                    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i>
                            </a>
                        </div>

                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <span class="m-r-sm text-muted welcome-message">Trang quản trị</span>
                            </li>
                            <li>
                                <a href="/logout">
                                    <i class="fa fa-sign-out"></i> Thoát
                                </a>
                            </li>
                        </ul>

                    </nav>
                </div>

                
                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-12">
                        <h2>Danh sách Từ khóa</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="/">Trang chủ</a>
                            </li>
                        </ol>
                    </div>
                </div>

                
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-content">

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <a href="http://cms.banhcuonphuonganh.com.vn/tag/create" class="btn btn-primary "><i class="fa fa-plus"></i> Thêm</a>
                                        </div>
                                    </div>

                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Từ khóa</th>
                                                <th>Slug</th>
                                                <th width="150"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>bánh cuốn</td>
                                                <td>banh-cuon</td>
                                                <td>
                                                    <a class="btn btn-white btn-sm m-l-3" href="http://cms.banhcuonphuonganh.com.vn/tag/1/edit" title="Sửa thông tin Từ khóa bánh cuốn">
                                                        <i class="fa fa-pencil"></i>
                                                        Sửa
                                                    </a>
                                                    <a class="btn btn-white btn-sm m-l-3 delete_item" href="http://cms.banhcuonphuonganh.com.vn/tag/1" title="Xóa Từ khóa bánh cuốn">
                                                        <i class="fa fa-trash"></i>
                                                        Xóa
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>chả mực</td>
                                                <td>cha-muc</td>
                                                <td>
                                                    <a class="btn btn-white btn-sm m-l-3" href="http://cms.banhcuonphuonganh.com.vn/tag/2/edit" title="Sửa thông tin Từ khóa chả mực">
                                                        <i class="fa fa-pencil"></i>
                                                        Sửa
                                                    </a>
                                                    <a class="btn btn-white btn-sm m-l-3 delete_item" href="http://cms.banhcuonphuonganh.com.vn/tag/2" title="Xóa Từ khóa chả mực">
                                                        <i class="fa fa-trash"></i>
                                                        Xóa
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="footer">
                        <div class="pull-right">
                            <strong>Liên hệ:</strong> 0973578633
                        </div>
                        <div>
                            <strong>Copyright</strong> HaoTT © 2017
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="http://cms.banhcuonphuonganh.com.vn/themes/inspinia/js/bootstrap.min.js"></script>
        <script src="http://cms.banhcuonphuonganh.com.vn/themes/inspinia/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="http://cms.banhcuonphuonganh.com.vn/themes/inspinia/js/plugins/flot/jquery.flot.js"></script>
        <script src="http://cms.banhcuonphuonganh.com.vn/themes/inspinia/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
        <script src="http://cms.banhcuonphuonganh.com.vn/themes/inspinia/js/inspinia.js"></script>
        <script src="http://cms.banhcuonphuonganh.com.vn/themes/inspinia/js/plugins/chartJs/Chart.min.js"></script>

        <script src="http://cms.banhcuonphuonganh.com.vn/assets/cms/js/app.js"></script>





    </body></html>