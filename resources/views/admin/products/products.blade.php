<html><head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="lvhcxQ5CGMX9ECofpi4JOUCr6IMMK5nE4B50eUex">

<title>Sản phẩm</title>

<link href="http://cms.banhcuonphuonganh.com.vn/themes/inspinia/css/bootstrap.min.css" rel="stylesheet">
<!-- <link href="/themes/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet"> -->
<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="http://cms.banhcuonphuonganh.com.vn/themes/inspinia/css/animate.css" rel="stylesheet">

<link href="http://cms.banhcuonphuonganh.com.vn/themes/inspinia/css/style.css" rel="stylesheet">
<link href="http://cms.banhcuonphuonganh.com.vn/assets/cms/css/style.css" rel="stylesheet">

<link rel="shortcut icon" href="favicon.ico">    <script src="http://cms.banhcuonphuonganh.com.vn/themes/inspinia/js/jquery-2.1.1.js"></script>
<script src="http://cms.banhcuonphuonganh.com.vn/assets/base.js"></script>

<script>
    var csrf_token = 'lvhcxQ5CGMX9ECofpi4JOUCr6IMMK5nE4B50eUex';
</script>

</head>

<body>
    <div id="wrapper">

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
<!--                    <span>
                            <img alt="image" class="img-circle" src="" width="48px"
                                 height="48px"/>
                             </span>-->
                             <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">Admin</strong>
                                </span> <span class="text-muted text-xs block">Thông tin tài khoản <b class="caret"></b></span> </span> </a>
                                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                    <li><a href="http://cms.banhcuonphuonganh.com.vn/user/7/edit">Thông tin cá nhân</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li><a href="/logout">Thoát</a></li>
                                </ul>
                            </div>
                            <div class="logo-element">
                                CMS
                            </div>
                        </li>
                        <li class="active">
                            <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Sản phẩm</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse in">
                                <li><a href="/product">Sản phẩm</a></li>
                                <li><a href="/product/category">Thể loại Sản phẩm</a></li>
                                <li><a href="/product/feature">Sản phẩm Chọn lọc</a></li>
                                <li><a href="/product/hot">Sản phẩm Hot</a></li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Bài viết</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="/article">Bài viết</a></li>
                                <li><a href="/article/category">Thể loại Bài viết</a></li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Tin tức</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="/news">Tin tức</a></li>
                                <li><a href="/news/category">Thể loại Tin tức</a></li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Người dùng</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="/user">Quản trị viên</a></li>
                                <li><a href="/group">Nhóm Quản trị</a></li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Thông tin khác</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="/menu">Menu</a></li>
                                <li><a href="/slide">Slide</a></li>
                                <li><a href="/tag">Từ khóa</a></li>
                                <li><a href="/faq">Hỏi đáp</a></li>
                                <li><a href="/config">Cấu hình</a></li>
                            </ul>
                        </li>
                    </ul>

                </div>
            </nav>
            <div id="page-wrapper" class="gray-bg dashbard-1">
                <div class="row border-bottom">

                    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i>
                            </a>
                        </div>

                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <span class="m-r-sm text-muted welcome-message">Trang quản trị</span>
                            </li>
                            <li>
                                <a href="/logout">
                                    <i class="fa fa-sign-out"></i> Thoát
                                </a>
                            </li>
                        </ul>

                    </nav>
                </div>

                
                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-12">
                        <h2>Danh sách Sản phẩm</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="/">Trang chủ</a>
                            </li>
                        </ol>
                    </div>
                </div>

                
                <div class="wrapper wrapper-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-content">

                                    <div class="row">
                                        <form method="GET" action="http://cms.banhcuonphuonganh.com.vn/product" accept-charset="UTF-8" id="search-frm" class="form-horizontal">
                                            <div class="col-lg-4">
                                                <div class="btn-group">
                                                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">Hành
                                                        động <span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="/product/feature" class="_check_add_to" data-target="_list"><i class="fa fa-plus"></i> Chọn lọc</a></li>
                                                            <li><a href="/product/hot" class="_check_add_to" data-target="_list"><i class="fa fa-plus"></i> Hot</a></li>
                                                        </ul>
                                                    </div>

                                                    <a href="http://cms.banhcuonphuonganh.com.vn/product/create" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm</a>
                                                </div>
                                                <div class="col-lg-2" style="padding-right: 0;">
                                                    <select class="form-control chosen-select" data-placeholder="Chọn Trạng thái" onchange="$(&quot;#search-frm&quot;).submit();" name="status"><option value="0" selected="selected">Chọn Trạng thái</option><option value="1">Xuất bản</option><option value="2">Chưa xuất bản</option></select>
                                                </div>
                                                <div class="col-lg-3" style="padding-right: 0;">
                                                    <select class="form-control chosen-select" data-placeholder="Chọn Thể loại" onchange="$(&quot;#search-frm&quot;).submit();" name="category_id"><option value="0" selected="selected">Chọn Thể loại</option><option value="1">Bánh cuốn Chả tôm</option><option value="3">Bánh cuốn chay</option><option value="2">Bánh cuốn nhân thịt</option></select>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="input-group">
                                                        <input class="form-control" placeholder="Tìm kiếm" autocomplete="off" name="keyword" type="text" value="">

                                                        <input type="hidden" name="action" id="search-action" value="id">

                                                        <div class="input-group-btn">
                                                            <button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button" aria-expanded="false"><i class="fa fa-search"></i>
                                                            </button>
                                                            <ul class="dropdown-menu pull-right">
                                                                <li><a href="#" onclick="$(&quot;#search-action&quot;).val(&quot;id&quot;);$(&quot;#search-frm&quot;).submit();">Tìm
                                                                    theo ID</a></li>
                                                                    <li><a href="#" onclick="$(&quot;#search-action&quot;).val(&quot;eid&quot;);$(&quot;#search-frm&quot;).submit();">Tìm
                                                                        theo ID Mã hóa</a>
                                                                    </li>
                                                                    <li class="divider"></li>
                                                                    <li><a href="#" onclick="$(&quot;#search-action&quot;).val(&quot;title&quot;);$(&quot;#search-frm&quot;).submit();">Tìm
                                                                        theo Tiêu đề</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <table id="_list" class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th><input type="checkbox" value="" class="check_all"></th>
                                                        <th>ID</th>
                                                        <th>Hình ảnh</th>
                                                        <th>Tiêu đề</th>
                                                        <th>Thể loại</th>
                                                        <th>Trạng thái</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                    <tr>
                                                        <td><input type="checkbox" value="4" name="check_item[]" class="check_item"></td>
                                                        <td>4</td>
                                                        <td width="150">
                                                            <img src="http://banhcuonphuonganh.com.vn/public/statics/banhcuon/img.thumb.product/0/0/0/4.jpg" alt="" width="150px" height="100px">
                                                        </td>
                                                        <td>
                                                            <a target="_blank" href="http://banhcuonphuonganh.com.vn/banh-cuon-cha-muc,p5eZxQ">
                                                                <h4>Bánh cuốn chả mực</h4></a>

                                                                <!--<p>Giá bán: 40000VNĐ</p>-->

                                                                <p>2017-02-25 15:36:57</p>
                                                            </td>
                                                            <td></td>
                                                            <td>Xuất bản</td>
                                                            <td width="150">
                                                                <a class="btn btn-white btn-bitbucket" href="http://cms.banhcuonphuonganh.com.vn/product/4" title="Xem" data-toggle="tooltip" data-placement="top" data-original-title="Xem"><i class="fa fa-search"></i></a>

                                                                <a class="btn btn-white btn-bitbucket" href="http://cms.banhcuonphuonganh.com.vn/product/4/edit" title="Sửa" data-toggle="tooltip" data-placement="top" data-original-title="Sửa"><i class="fa fa-pencil"></i></a>

                                                                <a class="btn btn-white btn-bitbucket delete_item" href="http://cms.banhcuonphuonganh.com.vn/product/4" title="Xóa" data-toggle="tooltip" data-placement="top" data-original-title="Xóa"><i class="fa fa-trash"></i></a>
                                                            </td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td><input type="checkbox" value="3" name="check_item[]" class="check_item"></td>
                                                            <td>3</td>
                                                            <td width="150">
                                                                <img src="http://banhcuonphuonganh.com.vn/public/statics/banhcuon/img.thumb.product/0/0/0/3.jpg" alt="" width="150px" height="100px">
                                                            </td>
                                                            <td>
                                                                <a target="_blank" href="http://banhcuonphuonganh.com.vn/banh-cuon-chay,qAP67e">
                                                                    <h4>Bánh cuốn chay</h4></a>

                                                                    <!--<p>Giá bán: 20000VNĐ</p>-->

                                                                    <p>2017-02-25 15:35:39</p>
                                                                </td>
                                                                <td>Bánh cuốn chay</td>
                                                                <td>Xuất bản</td>
                                                                <td width="150">
                                                                    <a class="btn btn-white btn-bitbucket" href="http://cms.banhcuonphuonganh.com.vn/product/3" title="Xem" data-toggle="tooltip" data-placement="top" data-original-title="Xem"><i class="fa fa-search"></i></a>

                                                                    <a class="btn btn-white btn-bitbucket" href="http://cms.banhcuonphuonganh.com.vn/product/3/edit" title="Sửa" data-toggle="tooltip" data-placement="top" data-original-title="Sửa"><i class="fa fa-pencil"></i></a>

                                                                    <a class="btn btn-white btn-bitbucket delete_item" href="http://cms.banhcuonphuonganh.com.vn/product/3" title="Xóa" data-toggle="tooltip" data-placement="top" data-original-title="Xóa"><i class="fa fa-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td><input type="checkbox" value="2" name="check_item[]" class="check_item"></td>
                                                                <td>2</td>
                                                                <td width="150">
                                                                    <img src="http://banhcuonphuonganh.com.vn/public/statics/banhcuon/img.thumb.product/0/0/0/2.jpg" alt="" width="150px" height="100px">
                                                                </td>
                                                                <td>
                                                                    <a target="_blank" href="http://banhcuonphuonganh.com.vn/banh-cuon-nhan-thit,oGXY0e">
                                                                        <h4>Bánh cuốn nhân thịt</h4></a>

                                                                        <!--<p>Giá bán: 25000VNĐ</p>-->

                                                                        <p>2017-02-25 15:33:30</p>
                                                                    </td>
                                                                    <td>Bánh cuốn nhân thịt</td>
                                                                    <td>Chưa xuất bản</td>
                                                                    <td width="150">
                                                                        <a class="btn btn-white btn-bitbucket" href="http://cms.banhcuonphuonganh.com.vn/product/2" title="Xem" data-toggle="tooltip" data-placement="top" data-original-title="Xem"><i class="fa fa-search"></i></a>

                                                                        <a class="btn btn-white btn-bitbucket" href="http://cms.banhcuonphuonganh.com.vn/product/2/edit" title="Sửa" data-toggle="tooltip" data-placement="top" data-original-title="Sửa"><i class="fa fa-pencil"></i></a>

                                                                        <a class="btn btn-white btn-bitbucket delete_item" href="http://cms.banhcuonphuonganh.com.vn/product/2" title="Xóa" data-toggle="tooltip" data-placement="top" data-original-title="Xóa"><i class="fa fa-trash"></i></a>
                                                                    </td>
                                                                </tr>
                                                                
                                                                <tr>
                                                                    <td><input type="checkbox" value="1" name="check_item[]" class="check_item"></td>
                                                                    <td>1</td>
                                                                    <td width="150">
                                                                        <img src="http://banhcuonphuonganh.com.vn/public/statics/banhcuon/img.thumb.product/0/0/0/1.jpg" alt="" width="150px" height="100px">
                                                                    </td>
                                                                    <td>
                                                                        <a target="_blank" href="http://banhcuonphuonganh.com.vn/banh-cuon-cha-tom,VqXbBX">
                                                                            <h4>Bánh cuốn Chả tôm</h4></a>

                                                                            <!--<p>Giá bán: 35000VNĐ</p>-->

                                                                            <p>2017-02-25 15:13:50</p>
                                                                        </td>
                                                                        <td>Bánh cuốn Chả tôm</td>
                                                                        <td>Chưa xuất bản</td>
                                                                        <td width="150">
                                                                            <a class="btn btn-white btn-bitbucket" href="http://cms.banhcuonphuonganh.com.vn/product/1" title="Xem" data-toggle="tooltip" data-placement="top" data-original-title="Xem"><i class="fa fa-search"></i></a>

                                                                            <a class="btn btn-white btn-bitbucket" href="http://cms.banhcuonphuonganh.com.vn/product/1/edit" title="Sửa" data-toggle="tooltip" data-placement="top" data-original-title="Sửa"><i class="fa fa-pencil"></i></a>

                                                                            <a class="btn btn-white btn-bitbucket delete_item" href="http://cms.banhcuonphuonganh.com.vn/product/1" title="Xóa" data-toggle="tooltip" data-placement="top" data-original-title="Xóa"><i class="fa fa-trash"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                            <div class="clearfix">
                                                                <div class="pull-right"></div>
                                                            </div>

                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="footer">
                                                <div class="pull-right">
                                                    <strong>Liên hệ:</strong> 0973578633
                                                </div>
                                                <div>
                                                    <strong>Copyright</strong> HaoTT © 2017
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <script src="http://cms.banhcuonphuonganh.com.vn/themes/inspinia/js/bootstrap.min.js"></script>
                                <script src="http://cms.banhcuonphuonganh.com.vn/themes/inspinia/js/plugins/metisMenu/jquery.metisMenu.js"></script>
                                <script src="http://cms.banhcuonphuonganh.com.vn/themes/inspinia/js/plugins/flot/jquery.flot.js"></script>
                                <script src="http://cms.banhcuonphuonganh.com.vn/themes/inspinia/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
                                <script src="http://cms.banhcuonphuonganh.com.vn/themes/inspinia/js/inspinia.js"></script>
                                <script src="http://cms.banhcuonphuonganh.com.vn/themes/inspinia/js/plugins/chartJs/Chart.min.js"></script>

                                <script src="http://cms.banhcuonphuonganh.com.vn/assets/cms/js/app.js"></script>





                            </body></html>