<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.products.products');
});

// Route::group(['prefix' => 'admin'], function(){
// 	Route::group(['prefix' => 'product'], function(){

// 	});
// });
Route::get('/product', function () {
    return view('admin.products.products');
});

Route::get('/product/category', function () {
    return view('admin.products.category');
});

Route::get('/product/feature', function () {
    return view('admin.products.feature');
});

Route::get('/product/hot', function () {
    return view('admin.products.category');
});

Route::get('/article', function () {
    return view('admin.articles.article');
});

Route::get('/article/category', function () {
    return view('admin.articles.category');
});

Route::get('/news', function () {
    return view('admin.news.news');
});

Route::get('/news/category', function () {
    return view('admin.news.category');
});

Route::get('/user', function () {
    return view('admin.user.user');
});

Route::get('/group', function () {
    return view('admin.user.group');
});

Route::get('/menu', function () {
    return view('admin.config.menu');
});

Route::get('/slide', function () {
    return view('admin.config.slide');
});

Route::get('/tag', function () {
    return view('admin.config.tag');
});

Route::get('/faq', function () {
    return view('admin.config.faq');
});

Route::get('/config', function () {
    return view('admin.config.config');
});

